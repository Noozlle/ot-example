import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

declare var TB:any;
declare var Cordova: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  session: any;
  publisher: any;
  apiKey: any;
  sessionId: string;
  token: string;
  constructor(public navCtrl: NavController) {
    this.apiKey = '46226462';
    this.sessionId = '2_MX40NjIyNjQ2Mn5-MTU0NTk0ODQzODU3OH5QS0V5ditPUk14Q01qSEF2dUExVjNVVC9-fg';
    this.token = 'T1==cGFydG5lcl9pZD00NjIyNjQ2MiZzaWc9MzgzODhjOGFjZDQ5OGM1ZTZjYmIxN2IxZTg0YjUzMzBmNTUwNzgyZTpzZXNzaW9uX2lkPTJfTVg0ME5qSXlOalEyTW41LU1UVTBOVGswT0RRek9EVTNPSDVRUzBWNWRpdFBVazE0UTAxcVNFRjJkVUV4VmpOVlZDOS1mZyZjcmVhdGVfdGltZT0xNTQ1OTQ4NDM4JnJvbGU9bW9kZXJhdG9yJm5vbmNlPTE1NDU5NDg0MzguNjQ4NjA1MTAyMTg2JmV4cGlyZV90aW1lPTE1NDY1NTMyMzgmY29ubmVjdGlvbl9kYXRhPSU3QiUyMnJvbGUlMjIlM0ElMjJvd25lciUyMiU3RA==';
  }
  startCall() {
    this.session = TB.initSession(this.apiKey, this.sessionId);
    this.session.on({
      streamCreated: (event) => {
        this.session.subscribe(event.stream, 'subscriber');
        TB.updateViews();
      },
      streamDestroyed: (event) => {
        console.log(`Stream ${event.stream.name} ended because ${event.reason}`);
        TB.updateViews();
      },

    });

    this.session.connect(this.token, () => {
      this.publisher = TB.initPublisher('publisher' , {insertMode: 'append'});
      this.session.publish(this.publisher);
      this.publisher.audioLevelUpdated(event , ()=>{
        console.log('audioLevelUpdated')
      })
      this.publisher.on("audioLevelUpdated", function(event){
        console.log('audioLevelUpdated' , event)
      })
    });
  }

}
